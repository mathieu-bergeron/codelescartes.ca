---
title: "Préface"
bookCollapseSection: false
weight: 1
bookNumbering: false
---

{{% pageTitle %}}

> Ce manuel a été développé entre avril 2022 et février 2023 suite à l'obtention
> d'une subvention de la fabriqueREL.



<table>

<tr>
<th>Version</th>
<td>

{{% now %}}

</td>
</tr>

<tr>
<th>Auteur</th>
<td>Mathieu Bergeron
</td>
</tr>

<tr>
<th>Remerciements</th>
<td>

Un grand merci à la <a href="https://fabriquerel.org/qui-sommes-nous/">fabriqueREL</a> d'avoir supporté la création de ce manuel

</td>
</tr>

<tr>
<th>Participation étudiante</th>
<td>

Grace au financement de la fabriqueREL, trois étudiants ont contribué au développement du <a href="https://github.com/cartesjava/ca.ntro.cards">logiciel</a> qui accompagne ce manuel: 
Adrien Joséphine-Olivier,
Marlond Augustin
et
Zein-El-Abdin Hazimeh



</td>
</tr>

<tr>
<th>Mise en page</th>
<td>

La mise en page repose sur le thème <a href="https://github.com/alex-shpak/hugo-book">hugo-book</a>, modifié par Mathieu Bergeron pour les besoins de ce manuel (p.ex. ajout de la numération des sections)
 

</td>
</tr>

<tr>
<th>Suivi de projet</th>
<td>

*De la fabriqueREL:* &nbsp;Claude Potvin (conseillier pédagogique) et Marianne Dubé (coordonatrice).
*Du Collège Montmorency:* &nbsp;Philippe Lavigueur (conseillier pédagogique) et Valérie Jacques (spécialiste en moyens et techniques d'enseignement).
</td>
</tr>


<tr>
<th>Comité de relecture</th>
<td>

*Enseignant.es en informatique au Collège Montmorency:* &nbsp;Soumaya Medini, Axel Seguin, Nicolas Leduc, Frédéric Édoukou, Simon Deschênes et Sylvain Labranche.
</td>
</tr>

<tr>
<th>Pour citer cet ouvrage</th>
<td>

Bergeron M (2023). *Code les cartes! Apprendre à programmer efficacement avec des cartes à jouer*.
Collège Montmorency. fabriqueREL. Licence CC BY-SA


</td>
</tr>

<tr>
<th><img src="/by-sa.png"></th>
<td>

<table>

<!--
<tr>
<th>
Licence CC
</th>
-->

<td colspan="2" style="text-align:center;">
Ce manuel peut-être partagé et adapté librement (<a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">licence CC BY-SA</a>)
</td>

</tr> 

<tr>
<th>
Attribution
</th>

<td>
SVP citer ce manuel si vous utiliez/modifiez son contenu
</td>

</tr>

<tr>
<th>
Partage dans les mêmes conditions
</th>

<td>
SVP utiliser la licence CC BY-SA si vous publiez du contenu adapté de ce manuel
</td>

</tr>

</table>


</td>
</tr>




</table>
