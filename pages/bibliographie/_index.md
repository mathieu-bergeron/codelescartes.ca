---
title: "Bibliographie"
weight: 85
bookNumbering: false
---

{{% pageTitle %}}

<table>

<tr>
<td id="aho-ullman-al-2020">
Alfred V. Aho, Jeffrey D. Ullman et al.
</td>
<td>

<a href="https://en.wikibooks.org/wiki/Data_Structures">
Data Structures : {fundamental tools} (revisé en 2020) [Wikibooks]
</a>
</td>
</tr>

<tr>
<td id="mayfield-downey-2020">
Chris Mayfield et Allen Downey
</td>
<td>
<a href="https://open.umn.edu/opentextbooks/textbooks/think-java-how-to-think-like-a-computer-scientist">
Think Java : How to Think Like a Computer Scientist (2e éd., 2020)
</a>
</td>
</tr>


</table>





