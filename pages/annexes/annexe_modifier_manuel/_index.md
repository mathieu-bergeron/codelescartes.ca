---
title: "Annexe 4: suggérer une modification à ce manuel"
bookCollapseSection: false
bookNumbering: false
weight: 300
---

# Comment modifier ce manuel

## Obtenir les sources

Le dépôt source est ici: https://github.com/cartesjava/cartesjava.github.io

## Suggérer une modification

1. Au bas de la page à modifier, cliquer sur *Suggérer une modification*

    <img class="figure" src="suggerer_modification.png" />

1. Se connecter à GitHub

1. La première fois uniquement

    * «forker» le dépôt

        <img class="figure" src="fork01.png"/>

1. Modifier le source de la page directement dans votre naviguateur

    <img class="figure" src="modifier01.png"/>

1. Décrire votre modification (commentaire de commit)

    <img class="figure" src="commit01.png"/>

1. Créer un «pull request» pour communiquer votre suggestion au propriétaire du dépôt d'origine

    * Étape 1

        <img class="figure" src="pull_request01.png"/>

    * Étape 2

        <img class="figure" src="pull_request02.png"/>

1. Attendre que le propriétaire du dépôt d'origine évalue votre suggestion


## Modifier le manuel en local

1. Installer <a href="https://gohugo.io/" target="_blank">Hugo</a> sur votre machine

1. Démarrer Hugo à la racine du dépôt Git `cartesjava.github.io`

    ```bash
    $ hugo server
    ```

1. Naviguer vers <a href="http://localhost:1313" target="blank">localhost:1313</a>


    
