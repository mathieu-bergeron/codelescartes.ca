---
title: "Annexe 1: la notation grand O"
weight: 90
bookNumbering: false
---

# La notation grand O

À venir. 

Pour l'instant, voir <a href="/bibliographie#aho-ullman-al-2020">Data Structures (Aho, Ullman et al; 2020)</a>, en particulier le <a href="https://en.wikibooks.org/wiki/Data_Structures/Asymptotic_Notation">chapitre 2</a>.
