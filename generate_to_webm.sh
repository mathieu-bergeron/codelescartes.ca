ag -g"mp4$" | while read i; 
do
    mp4="$i"; 
    webm=$(echo $mp4 | sed "s/.mp4/.webm/"); 
    if [ ! -e $webm ]; then
        cmd=$(echo ffmpeg -y -i "$mp4" "$webm"); 
        echo $cmd >> to_webm.sh
    fi
done
