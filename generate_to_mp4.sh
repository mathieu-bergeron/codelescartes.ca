rm -f to_mp4.sh
ag -g"webm$" | while read i; 
do
    webm="$i"; 
    mp4=$(echo $webm | sed "s/.webm/.mp4/"); 
    if [ ! -e $mp4 ]; then
        cmd=$(echo ffmpeg -y -i \"$webm\" \"$mp4\"); 
        echo $cmd >> to_mp4.sh
        cmd=$(echo rm -v \"$webm\"); 
        echo $cmd >> to_mp4.sh
    fi
done

ag "webm" -l | while read i; 
do
    source="$i"; 
    if [ "$source" != "to_mp4.sh" -a "$source" != "to_webm.sh" -a "$source" != "generate_to_mp4.sh" -a "$source" != "generate_to_webm.sh" ]; then

        cmd=$(echo sed \"s/webm/mp4/\" -i \"$source\"); 
        echo $cmd >> to_mp4.sh

        cmd=$(echo sed \"s-video/webm-video/mp4-\" -i \"$source\"); 
        echo $cmd >> to_mp4.sh
    fi
done
