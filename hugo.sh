draft=$1

if [ "$draft" != "" ]; then
    draft=" -D "
fi

hugo server --renderToDisk $draft
